# coreutils
alias c="clear"
alias v="vim"
alias please='sudo $(fc -ln -1)'
alias ls="ls --color=auto"
alias lc="ls --color=auto"
alias mkdir="mkdir -p"
alias cat="bat -p"
alias fcp='cpg -gR $(ls | fzf -m) "$@"' # fcp <dest dir>
alias fmv='mvg -g $(ls | fzf -m) "$@"'  # fmv <dest dir>
alias frm='rm -rf $(ls | fzf -m) "$@"'  # frm <selection>

# moreutils
alias nat="bat -n"
alias hg="hgrep --theme Nord"
alias reboot="sudo reboot"
alias goodbye="sudo poweroff"
alias xmerge="xrdb -merge ~/.Xresources"
alias wifi="sued /etc/wpa_supplicant.conf"
alias mnt="sudo mount -t msdos /dev/sd0e ~/.media && cd ~/.media"
alias umnt="cd && sudo umount -t msdos ~/.media"
alias cdfonts="cd /usr/pkg/share/fonts/X11 && ls"
alias listfonts="convert -list font | bat -p --theme=ansi"
alias cursors="bat -p /usr/X11R7/include/X11/cursorfont.h | awk 'NR==30,EOF {print $2}' | cut -d_ -f2-9"
alias showcursors="xfd -center -fn cursor"
alias h='fc -l -n -r 1 | sed -Ee "s/^[[:blank:]]+//" | nuniq | fzf | tr -d \\n | xclip -selection c'
alias peek="fzf --preview 'bat --color=always --theme=ansi --style=plain --line-range=:500 {}'"
alias shuf='mpv --no-audio-display $(shuffle *)'
alias fmpc="~/.git/github/fmui/./fmui"
alias ng="newsgroups"
alias lobsters="~/.git/github/lobsters/./lobsters"

# tribblix
alias pkg_add="/opt/local/sbin/pkg_add"
alias pkgin="/opt/local/bin/pkgin"
alias wifiup="sudo dladm connect-wifi -k mykey -e OPTUS_B5A779 wpi0"

# pkgin
alias search="pkgin search"
alias pkginstall="sudo pkgin install"
alias remove="sudo pkgin remove"
alias update="sudo pkgin update"
alias upgrade="sudo pkgin upgrade"
alias fupgrade="sudo pkgin full-upgrade"
alias autoremove="sudo pkgin autoremove"
alias pkgclean="sudo pkgin clean"

# pkgsrc
alias cvsup="cd ~/.pkgsrc/ && sudo cvs update -dP && cd"
alias wipup="cd ~/.pkgsrc/wip/ && git pull -r && cd"
alias miccdd="sudo make install clean clean-depends distclean"
alias sysupg="sudo sysupgrade auto https://nyftp.netbsd.org/pub/NetBSD-daily/netbsd-9/latest/amd64"

# meta
alias watch="mpv --geometry=700x393"
alias listen="mpv --no-video"
alias walls="nsxiv -t -p -b -r -g 1200x675 ~/Pictures/walls &"
alias dumps="nsxiv -t -p -b -r -g 1200x675 ~/Pictures/dumps &"
alias dumproot="sh ~/.scripts/dumproot.sh"
alias dumpframe="sh ~/.scripts/dumpframe.sh"

# banana
alias weather="~/.scripts/./wttr.sh"
alias moon="~/.scripts/./moon.sh"
alias mapscii="telnet mapscii.me"
alias todo="cat ~/Documents/todo/todo.txt"
alias todoed="vim ~/Documents/todo/todo.txt"

# ytfzf
alias yt="ytfzf -t"
alias ytdl="cd ~/Videos/youtube && ytfzf -td"
alias ytm="ytfzf -tm"
alias ytdlp="cd ~/Videos/youtube && yt-dlp -f 'ba'"
alias ytmdl="cd ~/Music/ytmdl && yt-dlp -f 'ba' -x --audio-format mp3 --split-chapters"

# kb-management
# view, edit and delete are set in shrc_functions
alias kbl="kb list -n"
alias kba="kb add" # kba -t TITLE -c CATEGORY -g "TAG1;TAG2"
alias kbt="kb list --tags"

# git
alias status="git status"
alias add="git add ."
alias commit="git commit -m"
alias push="git push"
alias pull="git pull"
