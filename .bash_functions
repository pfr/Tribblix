# shfm: cd on exit
s() {
    shfm "$@"
    cd "$(cat "${XDG_CACHE_HOME:=${HOME}/.cache}/shfm/exit")"
}

# make dir and go to it
md() {
    command mkdir -p $1
    cd $1
}

# copy to clipboard
copy() {
    xclip -selection clipboard
}

# KNOWLEDGE BASE: ~/.kb
# use fzf with kb
kbv() {
    cd $HOME/.kb ; \
    fzf --preview 'bat {}' --bind 'enter:become(bat {})' ; \
    cd
}

kbe() {
    cd $HOME/.kb ; \
    fzf --preview 'bat {}' --bind 'enter:become(vim {})' ; \
    cd
}

kbd() {
    cd $HOME/.kb ; \
    rm $(fzf --preview 'bat {}') ; \
    cd
}

kbn() {
    cd /$HOME/.kb ; \
    nvi ; \
    cd
}

# swap 2 filenames around, if they exist
# usage: swap <file1> <file2>
swap () {
    local TMPFILE=tmp.$$
    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}

# shuffle/play music within a directory
#shuf () {
#    mpv --no-audio-display $(shuffle *) 2> /dev/null | \
#    awk '/Playing/ { s = ""; for (i = 2; i <= NF; i++) s = s $i " "; \
#    cmd="(date +'%H:%M:%S')"; cmd | getline d; print d,"|",s; close(cmd)}'
#}

# ARCHIVE EXTRACTION
# usage: ex <file>
ex () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)    tar xjf $1    ;;
      *.tar.gz)     tar xzf $1    ;;
      *.tar.xz)     tar xf $1     ;;
      *.tar)        tar xf $1     ;;
      *.tar.zst)    uzstd $1      ;;
      *.bz2)        bunzip2 $1    ;;
      *.rar)        unrar x $1    ;;
      *.gz)         gunzip $1     ;;
      *.tbz2)       tar xjf $1    ;;
      *.tgz)        tar xzf $1    ;;
      *.zip)        unzip $1      ;;
      *.Z)          uncompress $1 ;;
      *.7z)         7z x $1       ;;
      *.deb)        ar x $1       ;;
      *)    echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Creates an archive (*.tar.gz) from given directory.
mktar() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }

# Make your directories and files access rights sane.
sanitize() { chmod -R u=rwX,g=rX,o= "$@" ;}

# Bourne-shell string quoting
# # Turns:
# # hello world   -> 'hello world'
# # hello 'world' -> 'hello '\''world'\''
# #
q () {
    printf '%s' "$d" | sed -Ee "s/'/'\\\\''/g; 1s/^/'/; \$s/\$/'/"
}

# change dir interectively
# courtesy of @rvp at UnitedBSD
ch () {
    local arg dirs d i

# shortcut-circuit
    if [ $# -eq 0 ]
    then    cd
            return
    fi

    arg="$1"
    for d in $(printf '%s' "$arg" | sed -Ee 's/[^/]+/&*/g')
    do      test -d "$d" || continue
            dirs="$dirs $(q $d)"
    done

    eval "set -- $dirs"

# no match
    if [ $# -eq 0 ]
    then    printf 1>&2 '%s: %s: no match\n' "$0" "$arg"
            return 1
    fi

# single match
    if [ $# -eq 1 ]
    then    cd "$1"
            return
    fi

# multiple matches
    i=0
    for d
    do      i=$((i + 1))
            printf '%d) %s\n' "$i" "$d"
    done
    printf '%s\n? ' '-----'
    read num

# do nothing if no number
    if [ "$num" = "" ]
    then    return 0
    fi

# invalid number
    if [ "$num" -lt 1 -o "$num" -gt $# ]
    then    printf 1>&2 '%s: %s: bad number\n' "$0" "$num"
            return 1
    fi

    eval "cd \"\${$num}\""
}
