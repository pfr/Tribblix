if [ -f ~/.bashrc ];
then
	. ~/.bashrc;
fi

export PATH=/usr/gnu/bin:/usr/bin:/usr/sbin:/sbin:/opt/local/bin
export PAGER="more -s"

PS1='[\u@\h:\w]\$ '
