#!/usr/bin/env bash

# To fix the " gpg: WARNING: unsafe permissions on homedir '/home/path/to/user/.gnupg' " error
# Make sure that the .gnupg directory and its contents is accessibile by your user.
chown -R $USER /home/$USER/.gnupg/

# Also correct the permissions and access rights on the directory
find /home/$USER/.gnupg -type f -exec chmod 600 {} \; # Set 600 for files
find /home/$USER/.gnupg -type d -exec chmod 700 {} \; # Set 700 for directories
