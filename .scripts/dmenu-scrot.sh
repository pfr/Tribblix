#!/usr/bin/env bash
#
#  TODO:
#   xclip isn't quite working properly

IMG_PATH=$HOME/Pictures/screenshots/
UL="pb 0x0"
TIME=3000 #Seconds notification should remain visible
FILENAME=$(date +%Y-%m-%d_@_%H-%M-%S-scrot.png)
ID=$(xprop -root 32x '\t$0' _NET_ACTIVE_WINDOW | awk {'print $2'})

prog="1.Fullscreen
2.Delayed
3.Section
4.Window
5.Upload
6.Delayed_upload
7.Section_upload"

cmd=$(dmenu  -l 20  -fn 'Sarasa Term CL Nerd Font-12' -nb '#181818' -nf '#ffd7af' -sb '#7f1e31' -sf '#ffd7af' -p 'Choose Screenshot Type'   <<< "$prog")

cd $IMG_PATH
case ${cmd%% *} in

	  1.Fullscreen) scrot -d 1 $FILENAME && notify-send -u low -t $TIME 'Scrot' 'Fullscreen taken and saved' ;;
	  2.Delayed) scrot -d 4 $FILENAME && notify-send -u low -t $TIME 'Scrot' 'Fullscreen Screenshot saved' ;;
	  3.Section) scrot -s $FILENAME && notify-send -u low -t $TIME 'Scrot' 'Screenshot of section saved' ;;
    4.Window) scrot -ub -d 1 $FILENAME && notify-send -i none -u low -t $TIME 'scrot' 'Window Screenshot saved' ;;
    5.Upload) scrot -d 1 $FILENAME && $UL $FILENAME | xclip && notify-send -u low -t $TIME "Screenshot" "Uploaded to $(xclip -o)" ;;
    6.Delayed_upload) scrot -d 4 $FILENAME && $UL $FILENAME | xclip && notify-send -u low -t $TIME "Screenshot" "Uploaded to $(xclip -o)" ;;
    7.Section_upload) scrot -s $FILENAME && $UL $FILENAME | xclip && notify-send -u low -t $TIME "Scrot" "Screenshot Uploaded to $(xclip -o)" ;;

  	*)		exec "'${cmd}'" ;;
esac
