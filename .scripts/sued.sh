#!/bin/sh

set -eu
umask 077
rm -f /tmp/sued-helper.sh	# prevent any symlink shenanigans
echo 'exec "${EDITOR:-vi}" "$@"' > /tmp/sued-helper.sh
exec su -m root /tmp/sued-helper.sh "$@"
