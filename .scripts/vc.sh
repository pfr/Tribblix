#!/bin/sh
#
# vc < view command

file_path=$(command -v "$1" 2>/dev/null)

if [ -z "$file_path" ]; then
    printf "%s\n" "Error: $1 not found"
    exit 1
fi

bat "$file_path"
